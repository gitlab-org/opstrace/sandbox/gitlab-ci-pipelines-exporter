# gitlab-ci-pipelines-exporter test

A demo of the public project [gitlab-ci-pipelines-exporter](https://github.com/mvisonneau/gitlab-ci-pipelines-exporter) in action.

The main purpose of the demo is to set this up with a hosted prometheus instance that we can query from GOUI.

See also https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2047.

## Requirements

- Docker (compose v2 is included in docker)
- aws-cli (if you want to deploy to AWS)

On Linux, to support `docker compose` ecs deployments, you may have to enabled [docker experimental features](https://github.com/docker/cli/blob/master/experimental/README.md).
Afterwards, restart the docker service.

## Setup

Get a `read_api` scoped token from GitLab and export it into the shell environment as `GCPE_GITLAB_TOKEN`.

Then run:

```shell
docker compose up
```

## Deploy

We are targetting the experiment [docker compose AWS ECS integration](https://docs.docker.com/cloud/ecs-integration/).

This assumes you have an AWS account with permission to manage ECS and relevant network services.
You must be able to authenticate with the aws CLI.

To summarise the steps required:

```bash
# this will ask for your AWS credentials or to select a profile
docker context create ecs myecscontext

# use the context
docker context use myecscontext

# deploy
docker compose up
```

From each stage you should see relevant output, including details of all created resources.

In your browser, navigate from the ECS cluster to the load balancers to find the auto-generated domain names for the services.

## Notes

In order to meet the limitations of docker ecs integration the compose file is non-typical.

ECS does not support host bind mounts via docker, so we inject the config via environment variables.
Luckily the config is minimal so this isn't problematic.

According to the [service docs](https://docs.docker.com/cloud/ecs-compose-examples/#service) non-standard http ports require special annotations.
I haven't found this to be the case, and the ports of both services are exposed on the load balancer domain.
